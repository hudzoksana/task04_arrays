package com.hudz.controller;

import com.hudz.model.Arrays;
import com.hudz.model.Game;

public class Controller {

    public final int[] getRandomDoor(final int doorsCount) {
       return Game.randomDoor(doorsCount);
    }

    public final int[][] allowGetPermutation(final int doorsCount) {
       return Game.getPermutation(doorsCount);
    }

    public final String heroWillDie(final int[] doors, final int power) {
        return String.valueOf(Game.willDie(doors, power));
    }

    public final void getSuccess(final int[] doors, final int power, final int[][] ways) {
            Game.getSuccess(doors, power, ways);
    }
    public final void doTask1() {
        Arrays.doTask1();
    }

    public final void doTask2() {
        Arrays.doTask2();
    }
}
