package com.hudz;

import com.hudz.view.ConsoleView;

public class Start {

    public static void main (String[] args) {

        ConsoleView consoleView = new ConsoleView();
        consoleView.start();
    }
}
