package com.hudz.model;
// Java program to print all
// permutations using Johnson
// and Trotter algorithm.
import java.util.*;
import java.lang.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Game {

    private static final Logger logger = LogManager.getLogger(Game.class.getName());
    private final static boolean LEFT_TO_RIGHT = true;
    private final static boolean RIGHT_TO_LEFT = false;

    // Utility functions for
    // finding the position
    // of largest mobile
    // integer in a[].
    private static int searchArr(final int[] a, final int n,
                                final int mobile) {
        for (int i = 0; i < n; i++) {
            if (a[i] == mobile) {
                return i + 1;
            }
        }
        return 0;
    }

    // To carry out step 1
    // of the algorithm i.e.
    // to find the largest
    // mobile integer.
    private static int getMobile(final int[] a,
                                final boolean dir[], final int n) {
        int mobile_prev = 0, mobile = 0;

        for (int i = 0; i < n; i++) {
            // direction 0 represents
            // RIGHT TO LEFT.
            if (dir[a[i] - 1] == RIGHT_TO_LEFT
                    && i != 0) {
                if (a[i] > a[i - 1] &&
                        a[i] > mobile_prev) {
                    mobile = a[i];
                    mobile_prev = mobile;
                }
            }

            // direction 1 represents
            // LEFT TO RIGHT.
            if (dir[a[i] - 1] == LEFT_TO_RIGHT
                    && i != n - 1) {
                if (a[i] > a[i + 1] &&
                        a[i] > mobile_prev) {
                    mobile = a[i];
                    mobile_prev = mobile;
                }
            }
        }

        if (mobile == 0
                && mobile_prev == 0) {
            return 0;
        } else {
            return mobile;
        }
    }

    // Prints a single
    // permutation
    private static int[] getOnePerm(final int[] a, final boolean[] dir,
                                   final int n) {
        int mobile = getMobile(a, dir, n);
        int pos = searchArr(a, n, mobile);

        // swapping the elements
        // according to the
        // direction i.e. dir[].
        if (dir[a[pos - 1] - 1] == RIGHT_TO_LEFT) {
            int temp = a[pos - 1];
            a[pos - 1] = a[pos - 2];
            a[pos - 2] = temp;
        } else if (dir[a[pos - 1] - 1] == LEFT_TO_RIGHT) {
            int temp = a[pos];
            a[pos] = a[pos - 1];
            a[pos - 1] = temp;
        }
        // changing the directions
        // for elements greater
        // than largest mobile integer.
        for (int i = 0; i < n; i++) {
            if (a[i] > mobile) {
                if (dir[a[i] - 1] == LEFT_TO_RIGHT) {
                    dir[a[i] - 1] = RIGHT_TO_LEFT;
                } else if (dir[a[i] - 1] == RIGHT_TO_LEFT) {
                    dir[a[i] - 1] = LEFT_TO_RIGHT;
                }
            }
        }
        return a;
    }

    // To end the algorithm
    // for efficiency it ends
    // at the factorial of n
    // because number of
    // permutations possible
    // is just n!.
    private static int fact(final int n) {
        int res = 1;

        for (int i = 1; i <= n; i++) {
            res = res * i;
        }
        return res;
    }

    // This function mainly
    // calls printOnePerm()
    // one by one to print
    // all permutations.
    public static int[][] getPermutation(final int n) {
         int[][] permutation = new int[fact(n)][n];

        // To store current
        // permutation
        int[] a = new int[n];

        // To store current
        // directions
        boolean[] dir = new boolean[n];

        // storing the elements
        // from 1 to n and
        // printing first permutation.
        for (int i = 0; i < n; i++) {
            a[i] = i + 1;
        }

        permutation[0] = a.clone();

        // initially all directions
        // are set to RIGHT TO
        // LEFT i.e. 0.
        for (int i = 0; i < n; i++) {
            dir[i] = RIGHT_TO_LEFT;
        }
        // for generating permutations
        // in the order.
        for (int i = 1; i < fact(n); i++) {
            permutation[i] = getOnePerm(a, dir, n).clone();
        }
        return permutation;
    }
    public static int[] randomDoor(final int n) {
        int[] array = new int[n];
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            if (random.nextBoolean()) {
                array[i] = (int) (Math.random() * (80 - 10 + 1) + 10);
            } else {
                array[i] = (int) ((-1) * Math.random() * (100 - 5 + 1) + 5);
            }
        }
        return array;
    }

    public static int willDie(final int[] doors, final int power) {
        int counter = 0;
        for (int i = 0; i < doors.length; i++) {
            if ((doors[i] + power) < 0) {
                counter += 1;
            }
        }
        return counter;
    }

    public static void getSuccess(final int[] doors, final int power, final int[][] ways) {
        boolean success;
        for (int i = 0; i < ways.length - 1; i++) {
            success = false;
            int wayPower = power;
            for (int j = 0; j < doors.length; j++) {
                wayPower += doors[ways[i][j] - 1];
                if (wayPower < 0) {
                    success = false;
                    break;
                } else {
                    success = true;
                }
            }
            if (success) {
                logger.trace(java.util.Arrays.toString(ways[i]));
            }
        }
    }
}