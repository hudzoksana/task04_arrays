package com.hudz.model;

import com.hudz.Start;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Arrays {
    static final int SIZE = 10;
    private static final Logger logger = LogManager.getLogger(Start.class.getName());

    public static void doTask1() {
        logger.trace("Task 1");
        int[] arr1 = new int[SIZE];
        int[] arr2 = new int[SIZE];

        for (int i = 0; i < SIZE; i++) {
            arr1[i] = (int) (Math.random() * 100);
            arr2[i] = (int) (Math.random() * 100);
        }
        logger.trace("Array 1:" + java.util.Arrays.toString(arr1));
        logger.trace("Array 2:" + java.util.Arrays.toString(arr2));

        int[] mutual = mutual(arr1, arr2);

        int l = 0;
        logger.trace("\nMutual elements: ");
        if (mutual.length != 0) {
            while (mutual[l] != 0) {
                logger.trace(mutual[l] + " ");
                l++;
            }
        } else {
            logger.trace("no such elements");
        }
    }

    public static int[] mutual(final int[] arr1, final int[] arr2) {

        int[] mutual = new int[SIZE];
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0, k = 0; j < SIZE; j++) {
                if (arr1[i] == arr2[j]) {
                    mutual[k] = arr1[i];
                    k++;
                }
            }
        }
        return mutual;
    }
    public static void doTask2() {
        logger.trace("\nTask 2");
        int[] arrTask2 = {1, 1, 1, 2, 1, 3, 3};
        int n = arrTask2.length;
        logger.trace("Our array:" + java.util.Arrays.toString(arrTask2));
        for (int i = 0, m = 0; i != n; i++, n = m) {
            for (int j = m = i + 1; j != n; j++) {
                if (arrTask2[j] != arrTask2[i]) {
                    if (m != j) {
                        arrTask2[m] = arrTask2[j];
                    }
                    m++;
                }
            }
        }
        if (n != arrTask2.length) {
            int[] result = new int[n];
            for (int i = 0; i < n; i++) {
                result[i] = arrTask2[i];
            }
            arrTask2 = result;
        }
        logger.trace("\nAfter deleting:" + java.util.Arrays.toString(arrTask2));
    }
}
