package com.hudz.view;

import com.hudz.Start;
import com.hudz.controller.Controller;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class ConsoleView {

    private static Controller controller = new Controller();
    private static final Logger logger = LogManager.getLogger(Start.class.getName());

    public static void start() {
        viewTaskGame();
        viewTaskArray();
    }

    public static void viewTaskGame() {
        logger.trace("Task with game!");
        final int power = 25;
        int doorsCount = 4;
        int[] doors = controller.getRandomDoor(doorsCount);
        //int[] doors = {500, -40, -80, -50};
        int[][] ways = controller.allowGetPermutation(doorsCount);
        logger.trace("Your power is " + power + ". Now we have " + doorsCount + " doors:");
        printDoors(doors);
        logger.trace("Death is waiting for you in " + controller.heroWillDie(doors, power) + " door(s)");
        logger.trace("You will be alive if you open doors:\n");
        controller.getSuccess(doors, power, ways);
    }
    public static void printDoors(final int[] doors) {
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] > 0) {
                logger.trace("Door #" + (i + 1) + " has artifact. Power: " + doors[i]);
            } else {
                logger.trace("Door #" + (i + 1) + " has monster! Power " + (-1) * doors[i]);
            }
        }
    }
    public static void viewTaskArray() {
       logger.trace("\n\nTask with arrays!");
        controller.doTask1();
        controller.doTask2();
    }

}
